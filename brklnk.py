#!/usr/bin/env python
#-*- coding: utf-8 -*

import argparse
from bs4 import BeautifulSoup
from urllib.parse import urljoin, urlparse, urlunparse
import requests

# final version

def breaklinks(url, depth):
    if depth <0:
        return
    result = requests.get(url)
    if result.status_code >= 400:
        print(f'The link {url} is broken.')
    else:
        src= result.content
        soup= BeautifulSoup(src,'lxml')
        links= soup.find_all("a")
        for link in links:
            link_parse = urlparse(link.get('href'))
            if  link_parse.scheme == '':
                link_correct = urljoin(url,link_parse.path)
            link_correct = urlunparse(link_parse)
            breaklinks(link_correct, depth-1)

def main():
    parser = argparse.ArgumentParser(description='Get all broken links')
    parser.add_argument('url', type = str, help="Search the url for broken links")
    parser.add_argument('--depth', type = int, help="Search depth (n = level 0")
    args = parser.parse_args()
    breaklinks(args.url, args.depth)


if __name__ == "__main__":
    main()